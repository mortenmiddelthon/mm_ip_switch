#!/usr/bin/perl 

use warnings;
use strict;

my $host = "10.0.0.100";
my $port = 80;

my $response;

$response = `echo "#status" |/bin/nc $host $port`;
my $relay1_status;
my $relay2_status;
if($response =~ /Relay 1 ([A-Z]*)/m) {
	$relay1_status = $1;
}
else {
	print "Failed to parse response!\n";
}
if($response =~ /Relay 2 ([A-Z]*)/m) {
	$relay2_status = $1;
}
else {
	print "Failed to parse response!\n";
}

print "Relay 1 status: $relay1_status\n";
print "Relay 2 status: $relay2_status\n";
