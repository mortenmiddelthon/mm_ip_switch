#define LEFTLEDS 6
#define RIGHTLEDS 9
#define HEAD 3
#define SWITCH 4
// Reed switch
#define REED 2
boolean switchState = false;

uint8_t brightness = 100;

// Wifi module setup
#include <Adafruit_CC3000.h>
#include <ccspi.h>
#include <SPI.h>
#include <string.h>
// #include "utility/debug.h"
#define ADAFRUIT_CC3000_IRQ   3  // MUST be an interrupt pin!
#define ADAFRUIT_CC3000_VBAT  5
#define ADAFRUIT_CC3000_CS    10
Adafruit_CC3000 cc3000 = Adafruit_CC3000(ADAFRUIT_CC3000_CS, ADAFRUIT_CC3000_IRQ, ADAFRUIT_CC3000_VBAT,
                                         SPI_CLOCK_DIVIDER); // you can change this clock speed
#define IDLE_TIMEOUT_MS  500

// Define WLAN SSID and key in the file wlan.h
#include "wlan.h"

// What page to grab!
#define WEBSITE      "10.0.0.40"
#define WEBPAGE      "/ip_switch/?"

uint32_t ip;
uint8_t macAddress[6];
// Unit ID is based on last part of MAC address
uint8_t id;

void setup(void)
{
	pinMode(LEFTLEDS, OUTPUT);
	pinMode(RIGHTLEDS, OUTPUT);
	Serial.begin(9600);
	Serial.println("[IP Switch]");

	pinMode(REED, INPUT);

	// Serial.print("Free RAM: "); Serial.println(getFreeRam(), DEC);
	
	/* Initialise the module */
	Serial.println(F("\nInitializing..."));
	if (!cc3000.begin())
	{
		Serial.println(F("Couldn't begin()! Check your wiring?"));
		while(1);
	}
	
	Serial.print(F("\nAttempting to connect to ")); Serial.println(WLAN_SSID);
	if (!cc3000.connectToAP(WLAN_SSID, WLAN_PASS, WLAN_SECURITY)) {
		Serial.println(F("Failed!"));
		while(1);
	}
	 
	Serial.println(F("Connected to WLAN!"));
	
	/* Wait for DHCP to complete */
	Serial.println(F("Request DHCP"));
	while (!cc3000.checkDHCP())
	{
		delay(100); // ToDo: Insert a DHCP timeout!
	}	

	/* Display the IP address DNS, Gateway, etc. */	
	while (! displayConnectionDetails()) {
		delay(1000);
	}

	ip = 0;
	// Try looking up the website's IP address
	Serial.print(WEBSITE); 
	Serial.print(F(" -> "));
	while (ip == 0) {
		if (! cc3000.getHostByName(WEBSITE, &ip)) {
			Serial.println(F("Couldn't resolve!"));
		}
		delay(500);
	}
	cc3000.printIPdotsRev(ip);
	Serial.println();
	// Get MAC address
	cc3000.getMacAddress(macAddress);
	Serial.print("MAC: ");
	Serial.print(macAddress[0], HEX);
	Serial.print(":");
	Serial.print(macAddress[1], HEX);
	Serial.print(":");
	Serial.print(macAddress[2], HEX);
	Serial.print(":");
	Serial.print(macAddress[3], HEX);
	Serial.print(":");
	Serial.print(macAddress[4], HEX);
	Serial.print(":");
	Serial.println(macAddress[5], HEX);
	id = macAddress[5];
	blink(2);
}
// Send a heartbeat every 60 seconds
uint32_t timestamp = millis();
uint32_t interval = 60000; // 60 seconds
void loop(void)
{
	char postString[12];
	String post;
	if(millis() > timestamp+interval) {
		postData("heartbeat=1");
		timestamp = millis();
	}
	uint16_t sw;
	uint16_t head;
	uint8_t reed = digitalRead(REED);
	sw = analogRead(SWITCH);
	head = analogRead(HEAD);
	if(sw > 250) {
		sw = 1;
	}
	if(head > 250) {
		timestamp = millis();
		head = 1;
		sprintf(postString, "sw0=%u&head=%u", sw, head);
		post = String(postString);
		postData(post);
	}
	if(reed == HIGH && switchState == false) {
		switchState = true;
		Serial.println("Magnet present");
		sprintf(postString, "sw0=%u&reed0=%u", sw, reed);
		post = String(postString);
		postData(post);
		delay(200);
	}
	else if(reed == LOW && switchState == true) {
		Serial.println("Magnet removed");
		switchState = false;
		sprintf(postString, "sw0=%u&reed0=%u", sw, reed);
		post = String(postString);
		postData(post);
		delay(200);
	}
	delay(50);
}

/**************************************************************************/
/*!
		@brief	Tries to read the IP address and other connection details
*/
/**************************************************************************/
bool displayConnectionDetails(void)
{
	uint32_t ipAddress, netmask, gateway, dhcpserv, dnsserv;
	
	if(!cc3000.getIPAddress(&ipAddress, &netmask, &gateway, &dhcpserv, &dnsserv))
	{
		Serial.println(F("Unable to retrieve the IP Address!\r\n"));
		return false;
	}
	else
	{
		Serial.print(F("\nIP Addr: ")); cc3000.printIPdotsRev(ipAddress);
		Serial.print(F("\nNetmask: ")); cc3000.printIPdotsRev(netmask);
		Serial.print(F("\nGateway: ")); cc3000.printIPdotsRev(gateway);
		Serial.print(F("\nDHCPsrv: ")); cc3000.printIPdotsRev(dhcpserv);
		Serial.print(F("\nDNSserv: ")); cc3000.printIPdotsRev(dnsserv);
		Serial.println();
		return true;
	}
}

void blink(uint8_t blinks) {
	uint8_t x;
	for(x = 0; x < blinks; x++) {
		analogWrite(LEFTLEDS, 0);
		analogWrite(RIGHTLEDS, 0);
		delay(100);
		analogWrite(LEFTLEDS, brightness);
		analogWrite(RIGHTLEDS, brightness);
		delay(100);
	}
}

void postData(String post) {
	String URI = WEBPAGE;
	analogWrite(LEFTLEDS, 0);
	analogWrite(RIGHTLEDS, 0);
	URI.concat(post);
	URI.concat("&ID=");
	URI.concat(id);
	char uriData[URI.length()+1];
	URI.toCharArray(uriData, URI.length()+1);
	Serial.print("URI: ");
	Serial.println(uriData);
	Serial.print("Connecting to: ");
	Serial.print(WEBSITE);
	Serial.print(" (");
	cc3000.printIPdotsRev(ip);
	Serial.println(")");
	Adafruit_CC3000_Client www = cc3000.connectTCP(ip, 80);
	if (www.connected()) {
		www.fastrprint(F("GET "));
		www.fastrprint(uriData);
		www.fastrprint(F(" HTTP/1.1\r\n"));
		www.fastrprint(F("Host: ")); www.fastrprint(WEBSITE); www.fastrprint(F("\r\n"));
		www.fastrprint(F("\r\n"));
		www.println();
	} else {
		Serial.println(F("Connection failed"));		
		blink(2);
		return;
	}

	Serial.println(F("-------------------------------------"));
	
	/* Read data until either the connection is closed, or the idle timeout is reached. */ 
	unsigned long lastRead = millis();
	while (www.connected() && (millis() - lastRead < IDLE_TIMEOUT_MS)) {
		while (www.available()) {
			char c = www.read();
			Serial.print(c);
			lastRead = millis();
		}
	}
	www.close();
	Serial.println(F("-------------------------------------"));
	
	/* You need to make sure to clean up after yourself or the CC3000 can freak out */
	/* the next time your try to connect ... */
	Serial.println(F("\n\nDisconnecting"));
	if(!post.equals("heartbeat")) {
		blink(2);
	}
	//cc3000.disconnect();
}


