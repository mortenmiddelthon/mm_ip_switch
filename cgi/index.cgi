#!/usr/bin/perl -w

use warnings;
use strict;
use CGI;
my $q = CGI->new;

my $host = "10.0.0.100";
my $port = 80;

print "Content-Type: text/plain\n\n";
print "Hello world!\n";

my @params = $q->param();
foreach my $p (@params) {
	print "$p: ";
	print $q->param($p);
	print "\n";
}

my $head = $q->param("head");
my $switch = $q->param("sw0");
my $reed = $q->param("reed0");

print "My head value: $head\n";
print "My switch value: $switch\n";
print "My reed switch value: $reed\n" if defined($reed);

my $response;

if($head == 1 or defined($reed)) {
	$response = `echo "#status" |/bin/nc $host $port`;
	my $relay1_status;
	my $relay2_status;
	if($response =~ /Relay 1 ([A-Z]*)/m) {
		$relay1_status = $1;
	}
	else {
		print "Failed to parse response!\n";
	}
	if($response =~ /Relay 2 ([A-Z]*)/m) {
		$relay2_status = $1;
	}
	else {
		print "Failed to parse response!\n";
	}
	
	print "Relay 1 status: $relay1_status\n";
	print "Relay 2 status: $relay2_status\n";
	if($switch == 0 and $relay1_status eq "OFF") {
		$response = `echo "#relay 1 on" |/bin/nc $host $port`;
	}
	if($switch == 0 and $relay1_status eq "ON") {
		$response = `echo "#relay 1 off" |/bin/nc $host $port`;
	}
	if($switch == 1 and $relay2_status eq "OFF") {
		$response = `echo "#relay 2 on" |/bin/nc $host $port`;
	}
	if($switch == 1 and $relay2_status eq "ON") {
		$response = `echo "#relay 2 off" |/bin/nc $host $port`;
	}
}

