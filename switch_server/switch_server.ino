
// Ethernet shield uses pin 10 -> 13
#include <SPI.h>
#include <Ethernet.h>
byte mac[] = {
	0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
};
IPAddress ip(10,0,0,100);
EthernetServer server(80);

// Relay pins
#define RELAY1 6
#define RELAY2 7

// Status LED and brightness level
#define STATUS 5
uint8_t brightness = 10;

void setup() {
	// Open serial communications and wait for port to open:
	Serial.begin(9600);
	while (!Serial) {
		; // wait for serial port to connect. Needed for Leonardo only
	}


	// start the Ethernet connection and the server:
	Ethernet.begin(mac, ip);
	server.begin();
	Serial.print("server is at ");
	Serial.println(Ethernet.localIP());

	// Setup relays. Default OFF
	pinMode(RELAY1, OUTPUT);
	pinMode(RELAY2, OUTPUT);
	digitalWrite(RELAY1, HIGH);
	digitalWrite(RELAY2, HIGH);
	pinMode(STATUS, OUTPUT);
}


void loop() {
	// listen for incoming clients
	EthernetClient client = server.available();
	// Store command
	String command;
	boolean readCommand = false;
	if (client) {
		Serial.println("new client");
		while (client.connected()) {
			if (client.available()) {
				char c = client.read();
				Serial.write(c);
				// Commands start with '#'
				if(c == '#') {
					readCommand = true;
				}
				// Read input into command string. Skip newlines
				else if(readCommand == true && c != '\n') {
					command.concat(c);
				}
				// Finish reading command when newline is received
				if(c == '\n' && readCommand == true) {
					if(readCommand == true) {
						command.trim();
						Serial.print("Command received: ");
						Serial.println(command);
						readCommand = false;
						// Check if it's a known command
						if(command.equals("help")) {
							printHelp(client);
						}
						else if(command.equals("quit") || command.equals("disconnect")) {
							client.println("Bye bye!");
							client.stop();
						}
						else if(command.startsWith("relay")) {
							if(relayControl(command)) {
								client.println(" -> Relay command OK");
							}
							else {
								client.println(" -> Relay command failed.");
							}
						}
						else if(command.equals("status")) {
							if(digitalRead(RELAY1) == LOW) {
								client.println(" -> Relay 1 ON");
							}
							else {
								client.println(" -> Relay 1 OFF");
							}
							if(digitalRead(RELAY2) == LOW) {
								client.println(" -> Relay 2 ON");
							}
							else {
								client.println(" -> Relay 2 OFF");
							}
						}
						else {
							client.print("Unrecognized command: \"");
							client.print(command);
							client.println("\"");
						}
						// Reset command string
						command = "";
						blink(2);

					}
					else {
						client.println("Invalid command");
						blink(3);
					}
				}
			}
		}
		// give the web browser time to receive the data
		delay(1);
		// close the connection:
		client.stop();
		Serial.println("client disconnected");
	}
}

void printHelp(EthernetClient client) {
	client.println("Help!");
}

void blink(uint8_t repeat) {
	uint8_t x;
	for(x = 0; x < repeat; x++) {
		analogWrite(STATUS, brightness);
		delay(100);
		digitalWrite(STATUS, LOW);
		delay(100);
	}
}

int relayControl(String command) {
	String sw = command.substring(6,7);
	String subCommand = command.substring(7);
	subCommand.trim();
	uint8_t myRelay = sw.toInt();
	if(myRelay == 1 || myRelay == 2) {
		if(subCommand.equals("on")) {
			if(myRelay == 1) {
				digitalWrite(RELAY1, LOW);
			}
			else if(myRelay == 2) {
				digitalWrite(RELAY2, LOW);
			}
		}
		else if(subCommand.equals("off")) {
			if(myRelay == 1) {
				digitalWrite(RELAY1, HIGH);
			}
			else if(myRelay == 2) {
				digitalWrite(RELAY2, HIGH);
				}
			}
		else {
			Serial.println("Invalid switch state");
			return 0;
		}
	}
	else {
		Serial.println("Invalid relay");
		return 0;
	}
	return 1;
}
